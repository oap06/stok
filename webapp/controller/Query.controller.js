sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/BusyIndicator",
    "sap/m/Token",
    "zcrm/ZCRM_PRDCT_STCK_QRY/controls/ExtScanner",
  ],
  function (
    Controller,
    Filter,
    FilterOperator,
    JSONModel,
    BusyIndicator,
    Token,
    ExtScanner
  ) {
    "use strict";

    return Controller.extend("zcrm.ZCRM_PRDCT_STCK_QRY.controller.Query", {
      onInit: function () {
        this.oScanner = new ExtScanner({
          settings: true,
          laser: true,
          valueScanned: this.onScanned.bind(this),
          decoderKey: "text",
          decoders: this.getDecoders(),
        });
      },
      // Barcode Scanner Begin
      onScanned: function (oEvent) {
        var that = this;
        var value = oEvent.getParameter("value");

        var oFilters = [];
        if (value !== "") {
          var oFilter = new Filter("Gtin", FilterOperator.EQ, value);
          oFilters.push(oFilter);
        }
        var oBusyDialog = new sap.m.BusyDialog();
        oBusyDialog.open();
        var oModelProd = this.getView().getModel("Prod");
        oModelProd.read("/productGtinSet", {
          filters: oFilters,
          success: function (oData, response) {
            try {
              var ProductID = oData.results[0].Product;
              that._setbarcode(ProductID);
            } catch (error) {}

            oBusyDialog.close();
            debugger;
          },
          error: function (oError) {
            oBusyDialog.close();
            debugger;
          },
        });
        // this.oMainModel.setProperty('/scannedValue', oEvent.getParameter('value'));
      },

      onScan: function () {
        this.oScanner.open();
      },

      getDecoders: function () {
        return [
          {
            key: "PDF417-UII",
            text: "PDF417-UII",
            decoder: this.parserPDF417UII,
          },
          {
            key: "text",
            text: "TEXT",
            decoder: this.parserText,
          },
        ];
      },

      parserText: function (oResult) {
        var sText = "";
        var iLength = oResult.text.length;
        for (var i = 0; i !== iLength; i++) {
          if (oResult.text.charCodeAt(i) < 32) {
            sText += " ";
          } else {
            sText += oResult.text[i];
          }
        }
        return sText;
      },

      parserPDF417UII: function (oResult) {
        // we expect that
        // first symbol of UII (S - ASCII = 83) or it just last group
        var sText = oResult.text || "";
        if (oResult.format && oResult.format === 10) {
          sText = "";
          var iLength = oResult.text.length;
          var aChars = [];
          for (var i = 0; i !== iLength; i++) {
            aChars.push(oResult.text.charCodeAt(i));
          }
          var iStart = -1;
          var iGRCounter = 0;
          var iGroupUII = -1;
          var sTemp = "";
          aChars.forEach(function (code, k) {
            switch (code) {
              case 30:
                if (iStart === -1) {
                  iStart = k;
                  sTemp = "";
                } else {
                  sText = sTemp;
                  iGRCounter = -1;
                }
                break;
              case 29:
                iGRCounter += 1;
                break;
              default:
                if (iGRCounter > 2 && code === 83 && iGRCounter > iGroupUII) {
                  sTemp = "";
                  iGroupUII = iGRCounter;
                }
                if (iGroupUII === iGRCounter) {
                  sTemp += String.fromCharCode(code);
                }
            }
          });
          if (sText) {
            sText = sText.slice(1);
          }
        }
        return sText;
      },

      _setbarcode: function (Barcode) {
        var input = this.getView().byId("ProductId");
        input.removeAllTokens();

        input.addToken(new Token({ text: Barcode, key: Barcode }));
      },

        // Barcode Scanner End

      onProdValueHelp: function (oEvent) {
        // ürün arama yardımı
        if (!this._oProdHelpDialog) {
          this._oProdHelpDialog = sap.ui.xmlfragment(
            "zcrm.ZCRM_PRDCT_STCK_QRY.fragments.ProdSearch",
            this
          );
          this.getView().addDependent(this._oProdHelpDialog);
          this._oProdHelpDialog.open();
        } else {
          this._oProdHelpDialog.open();
        }

        this.Source = oEvent.getSource();
      },

      handleProdCancel: function (oEvent) {
        this._oProdHelpDialog.close();
      },
      handleProdSearch: function (oEvent) {
        var oModelProd = this.getView().getModel("Prod");

        var oProdId = sap.ui.getCore().byId("idProdId").getValue();
        var oProdDes = sap.ui.getCore().byId("idProdDesc").getValue();
        var oCatId = sap.ui.getCore().byId("idCatId").getValue();
        var oCatDes = sap.ui.getCore().byId("idCatDesc").getValue();
        var oRow = sap.ui.getCore().byId("idProdRows").getValue();

        var oBusyDialog = new sap.m.BusyDialog();

        var oFilters = [];
        if (oProdId !== "") {
          var oFilter = new Filter("ProductId", FilterOperator.EQ, oProdId);
          oFilters.push(oFilter);
        }
        if (oProdDes !== "") {
          oFilter = new Filter("ShortText", FilterOperator.EQ, oProdDes);
          oFilters.push(oFilter);
        }
        if (oCatId !== "") {
          oFilter = new Filter("CategoryId", FilterOperator.EQ, oCatId);
          oFilters.push(oFilter);
        }
        if (oCatDes !== "") {
          oFilter = new Filter("CategoryDesc", FilterOperator.EQ, oCatDes);
          oFilters.push(oFilter);
        }
        if (oRow !== "") {
          oFilter = new Filter("MaxRows", FilterOperator.EQ, oRow);
          oFilters.push(oFilter);
        }

        var oModelJsonList = new sap.ui.model.json.JSONModel();
        var that = this;
        oBusyDialog.open();
        oModelProd.read("/productSearchSet", {
          filters: oFilters,
          success: function (oData, response) {
            oModelJsonList.setData(oData);
            that._oProdHelpDialog.setModel(oModelJsonList, "oProd");
            oBusyDialog.close();
          },
          error: function (oError) {
            oBusyDialog.close();
          },
        });
      },
      onCatValueHelp: function (oEvent) {
        // katalog arama yardımı
        if (!this._oCatHelpDialog) {
          this._oCatHelpDialog = sap.ui.xmlfragment(
            "zcrm.ZCRM_PRDCT_STCK_QRY.fragments.CatSearch",
            this
          );
          this.getView().addDependent(this._oCatHelpDialog);
          this._oCatHelpDialog.open();
        } else {
          this._oCatHelpDialog.open();
        }
      },

      handleCatCancel: function () {
        this._oCatHelpDialog.close();
      },
      handleCatSearch: function () {
        var oModelProd = this.getView().getModel("Prod");

        var oCatId = sap.ui.getCore().byId("idCategId").getValue();
        var oCatDes = sap.ui.getCore().byId("idCategDesc").getValue();
        var oHyrId = sap.ui.getCore().byId("idHyrType").getSelectedKey();
        var oHyrDes = sap.ui.getCore().byId("idHyrDes").getValue();
        var oRow = sap.ui.getCore().byId("idCatRows").getValue();

        var oBusyDialog = new sap.m.BusyDialog();

        var oFilters = [];
        if (oCatId !== "") {
          var oFilter = new Filter("CategoryId", FilterOperator.EQ, oCatId);
          oFilters.push(oFilter);
        }
        if (oCatDes !== "") {
          oFilter = new Filter("CategoryText", FilterOperator.EQ, oCatDes);
          oFilters.push(oFilter);
        }
        if (oHyrId !== "") {
          oFilter = new Filter("HierarchyId", FilterOperator.EQ, oHyrId);
          oFilters.push(oFilter);
        }
        if (oHyrDes !== "") {
          oFilter = new Filter("HierarchyText", FilterOperator.EQ, oHyrDes);
          oFilters.push(oFilter);
        }
        if (oRow !== "") {
          oFilter = new Filter("MaxRows", FilterOperator.EQ, oRow);
          oFilters.push(oFilter);
        }

        var oModelJsonList = new sap.ui.model.json.JSONModel();
        var that = this;
        oBusyDialog.open();
        oModelProd.read("/categorySearchSet", {
          filters: oFilters,
          success: function (oData, response) {
            oModelJsonList.setData(oData);
            that._oCatHelpDialog.setModel(oModelJsonList, "oCat");
            oBusyDialog.close();
          },
          error: function (oError) {
            oBusyDialog.close();
          },
        });
      },

      handleCatListItemPress: function (oEvent) {
        // katalog arama yardımından seçilen katalog ürün arama yardımındaki katalog alanına yazılır
        var oInd = oEvent
          .getParameter("listItem")
          .getBindingContextPath()
          .split("/")[2];
        var oCatid = sap.ui
          .getCore()
          .byId("CatTabId")
          .getItems()
          [oInd].getCells()[0]
          .getText();
        sap.ui.getCore().byId("idCatId").setValue(oCatid);
        this._oCatHelpDialog.close();
      },
      onCustomerValueHelp: function () {
        // müşteri arama yardımı
        if (!this._oValueHelpDialog) {
          this._oValueHelpDialog = sap.ui.xmlfragment(
            "zcrm.zcrm_sales_order_operation.fragments.CustomerSearch",
            this
          );
          this.getView().addDependent(this._oValueHelpDialog);
          this._oValueHelpDialog.open();
        } else {
          this._oValueHelpDialog.open();
        }
      },

      handleCustCancel: function () {
        this._oValueHelpDialog.close();
      },
      handleCustomerSearch: function () {
        var oModelCust = this.getView().getModel("Cust");
        var oPartner = sap.ui.getCore().byId("idCustIdent").getValue();
        var oAd = sap.ui.getCore().byId("idCustName").getValue();
        var oSoyad = sap.ui.getCore().byId("idCustLastName").getValue();
        var oTel = sap.ui.getCore().byId("idCustTel").getValue();
        var oTur = sap.ui.getCore().byId("idCustType").getSelectedKey();
        var oBusyDialog = new sap.m.BusyDialog();

        var oFilters = [];
        if (oPartner !== "") {
          var oFilter = new Filter("Partner", FilterOperator.EQ, oPartner);
          oFilters.push(oFilter);
        }
        if (oAd !== "") {
          oFilter = new Filter("Firstname", FilterOperator.EQ, oAd);
          oFilters.push(oFilter);
        }
        if (oSoyad !== "") {
          oFilter = new Filter("Lastname", FilterOperator.EQ, oSoyad);
          oFilters.push(oFilter);
        }
        if (oTel !== "") {
          oFilter = new Filter("Telephonemob", FilterOperator.EQ, oTel);
          oFilters.push(oFilter);
        }
        if (oTur !== "") {
          oFilter = new Filter("Partnertype", FilterOperator.EQ, oTur);
          oFilters.push(oFilter);
        }

        var oModelJsonList = new sap.ui.model.json.JSONModel();
        var that = this;
        oBusyDialog.open();
        oModelCust.read("/searchCustomerSet", {
          filters: oFilters,
          success: function (oData, response) {
            oModelJsonList.setData(oData);
            that._oValueHelpDialog.setModel(oModelJsonList, "oCustomer");
            oBusyDialog.close();
          },
          error: function (oError) {
            oBusyDialog.close();
          },
        });
      },

      handleCustomerListItemPress: function (oEvent) {
        // müşteri arama yardımından dönen id ve ad soyad alanları alınır
        var oInd = oEvent
          .getParameter("listItem")
          .getBindingContextPath()
          .split("/")[2];
        var oPartner = sap.ui
          .getCore()
          .byId("CustTabId")
          .getItems()
          [oInd].getCells()[0]
          .getText();
        var oPartnerName = sap.ui
          .getCore()
          .byId("CustTabId")
          .getItems()
          [oInd].getCells()[1]
          .getText();
        this.getView().byId("inpu2").setValue(oPartner);
        this.getView().byId("inpu201").setValue(oPartnerName);

        var oFilters = [];

        // seçilen müşsteriye göre müşteri adresi arama yardımı dolar
        var oFilter = new Filter("Code", FilterOperator.EQ, "0014");
        oFilters.push(oFilter);
        oFilter = new Filter("Extension", FilterOperator.EQ, this.oOrderType);
        oFilters.push(oFilter);
        oFilter = new Filter("Extension2", FilterOperator.EQ, oPartner);
        oFilters.push(oFilter);

        var oMAdres = this.getView().byId("inpu3");
        var oModelJsonList = new sap.ui.model.json.JSONModel();
        this.getView()
          .getModel()
          .read("/valueHelpSet", {
            filters: oFilters,
            success: function (oData, response) {
              oModelJsonList.setData(oData);
              oMAdres.setModel(oModelJsonList, "oMusteriAdres");
            },
            error: function (oError) {},
          });

        this._oValueHelpDialog.close();
      },

      handleCustEnter: function () {
        // müşteri elle girilirse adını soyadını bulmak için aşağıdaki entity çağırılır
        var oPartner = this.getView().byId("inpu2").getValue();
        var oModelCust = this.getView().getModel("Cust");
        var oFilters = [];

        if (oPartner !== "") {
          var oFilter = new Filter("Partner", FilterOperator.EQ, oPartner);
          oFilters.push(oFilter);
        }

        var that = this;

        oModelCust.read("/searchCustomerSet", {
          filters: oFilters,
          success: function (oData, response) {
            if (oData.results.length > 0) {
              var oPartnerName =
                oData.results[0].Firstname + " " + oData.results[0].Lastname;
              that.getView().byId("inpu201").setValue(oPartnerName);
            } else {
              that.getView().byId("inpu201").setValue("");
            }
          },
        });
        // müşteri adresi için
        oFilters = [];
        oFilter = new Filter("Code", FilterOperator.EQ, "0014");
        oFilters.push(oFilter);
        oFilter = new Filter("Extension", FilterOperator.EQ, this.oOrderType);
        oFilters.push(oFilter);
        oFilter = new Filter("Extension2", FilterOperator.EQ, oPartner);
        oFilters.push(oFilter);

        var oMAdres = this.getView().byId("inpu3");
        var oModelJsonList = new sap.ui.model.json.JSONModel();
        this.getView()
          .getModel()
          .read("/valueHelpSet", {
            filters: oFilters,
            success: function (oData, response) {
              oModelJsonList.setData(oData);
              oMAdres.setModel(oModelJsonList, "oMusteriAdres");
            },
            error: function (oError) {},
          });
      },
      handleProdListItemPress: function (oEvent) {
        // ürün arama yardımından bir satır seçilince ürün kodu ve metni yazılır.
        var oInd = oEvent
          .getParameter("listItem")
          .getBindingContextPath()
          .split("/")[2];
        var oProd = sap.ui
          .getCore()
          .byId("ProdTabId")
          .getItems()
          [oInd].getCells()[0]
          .getText();
        this.Source.setValue(oProd);
        // Ürünün metnini de tanım alanına basmamız gerekiyor.
        // var oProdText = sap.ui.getCore().byId("ProdTabId").getItems()[oInd].getCells()[1].getText();
        // var oTextId = this.Source.getId();
        // oTextId = oTextId.substr(0, 7) + "1" + oTextId.substr(8);
        // sap.ui.getCore().byId(oTextId).setValue(oProdText);
        this._oProdHelpDialog.close();
      },
      onSearch: function () {
        BusyIndicator.show(0);
        debugger;
        var ShipCond = this.getView().byId("ShipCond").getSelectedKey();

        var aId = this.getView()
          .byId("ProductId")
          .getTokens()
          .map(function getId(t) {
            return t.getKey();
          });
        if (this.getView().byId("ProductId").getValue()) {
          aId.push(this.getView().byId("ProductId").getValue());
        }
        var aFilter = aId.map(function (sId) {
          return new Filter({
            path: "ProductId",
            value1: sId,
            operator: FilterOperator.EQ,
          });
        });

        aFilter.push(
          new Filter({
            path: "ShipCond",
            value1: ShipCond,
            operator: FilterOperator.EQ,
          })
        );

        // var ProductId = this.getView().byId("ProductId").getValue();
        // var oFilters = [new Filter({
        // 		path: "ProductId",
        // 		value1: ProductId,
        // 		comparator: function (value, input) {
        // 			return value.length < input ? 0 : -1;
        // 		},
        // 		operator: FilterOperator.EQ
        // 	}),
        // 	new Filter({
        // 		path: "ShipCond",
        // 		value1: ShipCond,
        // 		comparator: function (value, input) {
        // 			return value.length < input ? 0 : -1;
        // 		},
        // 		operator: FilterOperator.EQ
        // 	})
        // ];
        var oModelProd = this.getView().getModel("Prod");
        this.getView().setModel(new JSONModel({}), "Stock");
        oModelProd.read("/productStockQuerySet", {
          filters: aFilter,
          success: function (oData, response) {
            this.getView().getModel("Stock").setData(oData.results);
            BusyIndicator.hide();
          }.bind(this),
          error: function (oError) {
            BusyIndicator.hide();
          },
        });
      },
      // handleProdDetailItem: function (oEvent) {
      // 	debugger;

      // },
      handleProductDetail: function (oEvent) {
        var ProdId = oEvent
          .getSource()
          .getBindingContext("Stock")
          .getProperty("ProductNo");
        var aFilter = [];
        if (ProdId) {
          aFilter.push(new Filter("ProductId", FilterOperator.EQ, ProdId));
        }
        if (!this._oDialog) {
          this._oDialog = sap.ui.xmlfragment(
            "zcrm.ZCRM_PRDCT_STCK_QRY.fragments.ProdDetail",
            this
          );
          this.getView().addDependent(this._oDialog);
          this._oDialog.open();
        } else {
          this._oDialog.open();
        }

        var oTable = sap.ui.getCore().byId("ProdDetail");
        var oBinding = oTable.getBinding("items");
        oBinding.filter(aFilter);
      },
      handleProdDetailCancel: function () {
        this._oDialog.close();
      },
      handlePopoverProd: function (oEvent) {
        var Img = oEvent.getSource().getSrc();
        this.getView().getModel("local").setData({ Img: Img });
        if (!this._oPopover) {
          this._oPopover = sap.ui.xmlfragment(
            "zcrm.ZCRM_PRDCT_STCK_QRY.fragments.ProdImage",
            this
          );
          this.getView().addDependent(this._oPopover);
        }

        var oButton = oEvent.getSource();
        jQuery.sap.delayedCall(0, this, function () {
          this._oPopover.openBy(oButton);
        });
      },
      handleProdOk: function (oEvent) {
        var ProductId = sap.ui
          .getCore()
          .byId("ProdTabId")
          .getSelectedContexts()
          .map(function (c) {
            return c.getProperty("ProductId");
          });
        var i, sToken, input;
        input = this.getView().byId("ProductId");
        input.removeAllTokens();
        if (ProductId.length) {
          for (i = 0; i < ProductId.length; i++) {
            sToken = ProductId[i];
            input.addToken(new Token({ text: sToken, key: sToken }));
          }
        }

        this._oProdHelpDialog.close();
      }

    });
  }
);
