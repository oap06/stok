/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require(["zcrm/zcrmprdctstckqry/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});
