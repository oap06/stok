/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"zcrm/zcrm_prdct_stck_qry/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});
